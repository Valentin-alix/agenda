# Routes :

### POST : /login
- username: str
- password: str
### POST : /logout
### POST : /account
- username: str
- password: str
### DELETE : /accounts
- id: int
### GET : /peoples
### GET : /peoples/id: int
### POST : /peoples
### DELETE : /peoples/id : int
### PUT : /peoples/id
- name?: str
- coordinates?: str
