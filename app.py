import json
from flask import Flask, Response, request

app = Flask(__name__)

@app.route("/login", methods=['POST'])
def login():
    data = json.loads(request.get_data())
    with open('agenda.json') as file:
        user = list(filter(lambda x : x['username'] == data['username'], file['users']))
        if user['password'] == data['password']:
            return Response("Connected", status=200)
        else:
            return Response(status=401)

@app.route("/logout", methods=['POST'])
def logout():
    data = json.loads(request.get_data())
    # TODO
    return "Disconnected"

@app.route("/account", methods=['POST'])
def create_account():
    # TODO CREATE USER
    data = json.loads(request.get_data())

@app.route("/account/<id>", methods=['DELETE'])
def delete_account_by_id(account_id: int):
    # TODO DELETE USER BY ID
    data = json.loads(request.get_data())

@app.route("/peoples", methods=['GET', 'POST'])
def get_or_post_people():
    if request.method == "GET":
        with open('agenda.json') as file:
            return Response(str(json.load(file)['users']), status=200)
    elif request.method == "POST":
        data = json.loads(request.get_data())
        # TODO CREATE PEOPLE
        return 'create people'

@app.route("/peoples/<id>", methods=['GET', 'DELETE'])
def get_or_delete_people(id):
    if request.method == 'DELETE':
        with open('agenda.json') as file:
            data = json.loads(request.get_data())
            file_json =  json.load(file)
            people = list(filter(lambda x : x['id'] == int(id), file_json['peoples']))
            # TODO
            return 'Deleted'
    elif request.method == "GET":
        try:
            with open('agenda.json') as file:
                file_json =  json.load(file)
                return Response(str(list(filter(lambda x : x['id'] == int(id), file_json['peoples']))), status=200)
        except ValueError:
            return Response("Invalid id, please enter integer", status=401)

@app.route("/peoples/<id>/<name>", methods=['PUT'])
def put_name_by_people_id(people_id: int, name: str):
    # TODO
    pass

@app.route("/peoples/<id>/<coordinates>", methods=['PUT'])
def put_coordinates_by_people_id(people_id: int, coordinates: str):
    # TODO
    pass

if __name__ == "__main__":
    app.run(debug=True)